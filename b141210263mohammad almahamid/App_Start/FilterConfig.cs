﻿using System.Web;
using System.Web.Mvc;

namespace b141210263mohammad_almahamid
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
