﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using b141210263mohammad_almahamid.Models.Data;
using System.Linq;

namespace b141210263mohammad_almahamid.Controllers
{

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            using (mahamid context = new mahamid())
            {
                AnasayfaOlan anaSayfa = new AnasayfaOlan();
                anaSayfa.Fotograflar = context.Foto.ToList();
          
                return View(anaSayfa);
            }
        }
       

        public ActionResult About()
        {

            return View();
        }

        public ActionResult Contact()
        {


            return View();
        }

        public ActionResult blank()
        {


            return View();
        }

        public ActionResult email()
        {


            return View();
        }
        public ActionResult timeline()
        {


            return View();
        }
        public ActionResult Calendar()
        {


            return View();
        }
        public ActionResult Notes()
        {


            return View();
        }
        public ActionResult file()
        {


            return View();
        }
        public ActionResult elemets()
        {


            return View();
        }
        public ActionResult buttons()
        {


            return View();
        }
        public ActionResult icons()
        {


            return View();
        }
        public ActionResult typography()
        {


            return View();
        }
        public ActionResult grid()
        {


            return View();
        }
        public ActionResult panels()
        {


            return View();
        }
        public ActionResult formselement()
        {


            return View();
        }
        public ActionResult formvalidation()
        {

            return View();
        }
        public ActionResult wysihtml()
        {


            return View();
        }
        public ActionResult fileupload()
        {


            return View();
        }
        public ActionResult image()
        {


            return View();
        }
        public ActionResult basictables()
        {


            return View();
        }
        public ActionResult datatables()
        {


            return View();
        }
        public ActionResult charts()
        {


            return View();
        }
        public ActionResult gauges()
        {


            return View();
        }
        public ActionResult vectormaps()
        {


            return View();
        }
        public ActionResult range()
        {


            return View();
        }
        public ActionResult page()
        {


            return View();
        }
        public ActionResult elfinder()
        {


            return View();
        }
        public ActionResult googlemaps()
        {


            return View();
        }
        public ActionResult braio()
        {


            return View();
        }
        public ActionResult signup()
        {


            return View();
        }

        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
       
        }
 public class AnasayfaOlan
        {
            public List<Foto> Fotograflar { get; set; }
         }

    }


