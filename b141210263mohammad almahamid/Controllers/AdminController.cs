﻿using b141210263mohammad_almahamid.Models.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace b141210263mohammad_almahamid.Controllers
{
    public class AdminController : Controller
    {

        #region // Fotograflar

        public ActionResult Fotograflar()
        {
            using (mahamid context = new mahamid())
            {
                var Fotograflar = context.Foto.ToList();
                return View(Fotograflar);
            }
        }
        public ActionResult FotografEkle()
        {
            return View();
        }
        public ActionResult FotografDuzenle(int FotografID)
        {
            using (mahamid context = new mahamid())
            {
                var _FotografDuzenle = context.Foto.Where(x => x.FotoID == FotografID).FirstOrDefault();
                return View(_FotografDuzenle);
            }
        }
        public ActionResult FotografSil(int FotografID)
        {
            try
            {
                using (mahamid context = new mahamid())
                {
                    context.Foto.Remove(context.Foto.First(d => d.FotoID == FotografID));
                    context.SaveChanges();
                    return RedirectToAction("Fotograflar", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        [HttpPost]
        public ActionResult FotografEkle(Foto s, HttpPostedFileBase file)
        {
            try
            {
                using (mahamid context = new mahamid())
                {
                    Foto _Fotograf = new Foto();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _Fotograf.FotoImage = memoryStream.ToArray();
                    }
                    _Fotograf.FotoAD = s.FotoAD;

                    context.Foto.Add(_Fotograf);
                    context.SaveChanges();
                    return RedirectToAction("Fotograflar", "Admin");
                }
            }
#pragma warning disable 0168
            catch (AmbiguousMatchException exception)
#pragma warning restore 0168
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult FotografDuzenle(Foto Fotograf, HttpPostedFileBase file)
        {
            try
            {
                using (mahamid context = new mahamid())
                {
                    var _FotografDuzenle = context.Foto.Where(x => x.FotoID == Fotograf.FotoID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _FotografDuzenle.FotoImage = memoryStream.ToArray();
                    }
                    _FotografDuzenle.FotoAD = Fotograf.FotoAD;
                    
                    context.SaveChanges();
                    return RedirectToAction("Fotograflar", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion

    }
}