﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(b141210263mohammad_almahamid.Startup))]
namespace b141210263mohammad_almahamid
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
