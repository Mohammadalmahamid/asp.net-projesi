namespace b141210263mohammad_almahamid.Models.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Foto")]
    public partial class Foto
    {
        public int FotoID { get; set; }

        [Column(TypeName = "image")]
        public byte[] FotoImage { get; set; }

        [Required]
        [StringLength(250)]
        public string FotoAD { get; set; }
    }
}
